FROM node:latest 

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . . 

# COPY html_pages/ /html_pages/

EXPOSE 3000

CMD [ "node", "index.js" ]
